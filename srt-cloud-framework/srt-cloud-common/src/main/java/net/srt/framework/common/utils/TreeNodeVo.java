package net.srt.framework.common.utils;

import lombok.Data;

import java.util.List;

/**
 * @ClassName TreeListVo
 * @Author zrx
 * @Date 2022/11/14 14:06
 */
@Data
public class TreeNodeVo {
	private Long id;
	private Long parentId;
	private Integer ifLeaf;
	//作业类型
	private Long taskId;
	private Integer taskType;
	private String parentPath;
	private String path;
	private Integer orderNo;
	private String label;
	private String name;
	private String description;
	private Long projectId;
	private List<TreeNodeVo> children;
	/**
	 * 自定义属性
	 */
	private Object attributes;
	/**
	 * 自定义类型
	 */
	private Object type;
}
