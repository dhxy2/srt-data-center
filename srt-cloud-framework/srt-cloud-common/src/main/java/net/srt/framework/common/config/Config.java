package net.srt.framework.common.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 项目配置信息的工具类
 *
 * @author zrx
 */
@Getter
@Component("config")
public class Config {

	@Value("${spring.datasource.driver-class-name}")
	private String driver;
	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.password}")
	private String password;

	@Value("${srtDataWarehouse.driverClassName}")
	private String houseDriveClassName;
	@Value("${srtDataWarehouse.dbName}")
	private String houseDbName;
	@Value("${srtDataWarehouse.url}")
	private String houseUrl;
	@Value("${srtDataWarehouse.username}")
	private String houseUsername;
	@Value("${srtDataWarehouse.password}")
	private String housePassword;
	@Value("${srtDataWarehouse.dbProjectName}")
	private String dbProjectName;
	@Value("${srtDataWarehouse.dbProjectUrl}")
	private String dbProjectUrl;
	@Value("${srtDataWarehouse.dbProjectUsername}")
	private String dbProjectUsername;

	public String getDbProjectNameById(Long projectId) {
		return dbProjectName.replace("{projectId}", projectId.toString());
	}

	public String getDbProjectUsernameById(Long projectId) {
		return dbProjectUsername.replace("{projectId}", projectId.toString());
	}

	public String getDbProjectUrlByName(String dbProjectName) {
		return dbProjectUrl.replace("{dbProjectName}", dbProjectName);
	}

}
