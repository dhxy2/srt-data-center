package net.srt.framework.common.cache;

/**
 * Redis Key管理
 *
 * @author 阿沐 babamu@126.com
 */
public class RedisKeys {

	/**
	 * 验证码Key
	 */
	public static String getCaptchaKey(String key) {
		return "sys:captcha:" + key;
	}

	/**
	 * accessToken Key
	 */
	public static String getAccessTokenKey(String accessToken) {
		return "sys:access:" + accessToken;
	}

	/**
	 * accessToken Key
	 */
	public static String getProjectIdKey(String accessToken) {
		return "sys:project:id:" + accessToken;
	}

	/**
	 * projectId Key
	 */
	public static String getProjectKey(Long projectId) {
		return "sys:project:" + projectId;
	}

}
