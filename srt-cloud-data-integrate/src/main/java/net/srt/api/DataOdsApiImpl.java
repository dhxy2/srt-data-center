package net.srt.api;

import lombok.RequiredArgsConstructor;
import net.srt.api.module.data.integrate.DataAccessApi;
import net.srt.api.module.data.integrate.DataOdsApi;
import net.srt.api.module.data.integrate.dto.DataAccessDto;
import net.srt.api.module.data.integrate.dto.DataAccessTaskDto;
import net.srt.api.module.data.integrate.dto.DataOdsDto;
import net.srt.constants.YesOrNo;
import net.srt.convert.DataAccessConvert;
import net.srt.convert.DataAccessTaskConvert;
import net.srt.convert.DataOdsConvert;
import net.srt.entity.DataAccessEntity;
import net.srt.entity.DataAccessTaskDetailEntity;
import net.srt.entity.DataAccessTaskEntity;
import net.srt.entity.DataOdsEntity;
import net.srt.framework.common.utils.Result;
import net.srt.service.DataAccessService;
import net.srt.service.DataAccessTaskDetailService;
import net.srt.service.DataAccessTaskService;
import net.srt.service.DataOdsService;
import org.springframework.web.bind.annotation.RestController;
import srt.cloud.framework.dbswitch.data.domain.DbSwitchResult;
import srt.cloud.framework.dbswitch.data.domain.DbSwitchTableResult;
import srt.cloud.framework.dbswitch.data.util.BytesUnitUtils;

import java.util.Date;
import java.util.List;

/**
 * @ClassName DataAccessApiImpl
 * @Author zrx
 * @Date 2022/10/26 11:50
 */
@RestController
@RequiredArgsConstructor
public class DataOdsApiImpl implements DataOdsApi {

	private final DataOdsService dataOdsService;

	@Override
	public Result<String> addOds(DataOdsDto dataOdsDto) {
		DataOdsEntity dataOdsEntity = dataOdsService.getByTableName(dataOdsDto.getProjectId(), dataOdsDto.getTableName());
		if (dataOdsEntity == null) {
			dataOdsService.save(DataOdsConvert.INSTANCE.convertByDto(dataOdsDto));
		} else {
			dataOdsDto.setId(dataOdsEntity.getId());
			dataOdsService.updateById(DataOdsConvert.INSTANCE.convertByDto(dataOdsDto));
		}
		return Result.ok();
	}
}
