package net.srt.api;

import lombok.RequiredArgsConstructor;
import net.srt.api.module.data.integrate.DataAccessApi;
import net.srt.api.module.data.integrate.dto.DataAccessDto;
import net.srt.api.module.data.integrate.dto.DataAccessTaskDto;
import net.srt.constants.YesOrNo;
import net.srt.convert.DataAccessConvert;
import net.srt.convert.DataAccessTaskConvert;
import net.srt.entity.DataAccessEntity;
import net.srt.entity.DataAccessTaskDetailEntity;
import net.srt.entity.DataAccessTaskEntity;
import net.srt.framework.common.utils.Result;
import net.srt.service.DataAccessService;
import net.srt.service.DataAccessTaskDetailService;
import net.srt.service.DataAccessTaskService;
import org.springframework.web.bind.annotation.RestController;
import srt.cloud.framework.dbswitch.data.domain.DbSwitchResult;
import srt.cloud.framework.dbswitch.data.domain.DbSwitchTableResult;
import srt.cloud.framework.dbswitch.data.util.BytesUnitUtils;

import java.util.Date;
import java.util.List;

/**
 * @ClassName DataAccessApiImpl
 * @Author zrx
 * @Date 2022/10/26 11:50
 */
@RestController
@RequiredArgsConstructor
public class DataAccessApiImpl implements DataAccessApi {

	private final DataAccessService dataAccessService;
	private final DataAccessTaskService dataAccessTaskService;
	private final DataAccessTaskDetailService dataAccessTaskDetailService;

	@Override
	public Result<DataAccessDto> getById(Long id) {
		DataAccessEntity dataAccessEntity = dataAccessService.loadById(id);
		return Result.ok(DataAccessConvert.INSTANCE.convertDto(dataAccessEntity));
	}

	@Override
	public Result<Long> addTask(DataAccessTaskDto dataAccessTaskDto) {
		DataAccessTaskEntity dataAccessTaskEntity = DataAccessTaskConvert.INSTANCE.convertByDto(dataAccessTaskDto);
		dataAccessTaskService.save(dataAccessTaskEntity);
		//更新任务的最新开始时间和状态
		dataAccessService.updateStartInfo(dataAccessTaskDto.getDataAccessId());
		return Result.ok(dataAccessTaskEntity.getId());
	}

	@Override
	public void updateTask(DataAccessTaskDto dataAccessTaskDto) {
		DataAccessTaskEntity dataAccessTaskEntity = DataAccessTaskConvert.INSTANCE.convertByDto(dataAccessTaskDto);
		dataAccessTaskService.updateById(dataAccessTaskEntity);
		//更新任务的最新结束时间和状态
		dataAccessService.updateEndInfo(dataAccessTaskDto.getDataAccessId(), dataAccessTaskDto.getRunStatus(), dataAccessTaskDto.getNextRunTime());
	}

	@Override
	public void addTaskDetail(Long projectId, Long taskId, Long dataAccessId, DbSwitchTableResult tableResult) {
		dataAccessTaskDetailService.save(DataAccessTaskDetailEntity.builder().projectId(projectId).taskId(taskId).dataAccessId(dataAccessId)
				.sourceSchemaName(tableResult.getSourceSchemaName()).sourceTableName(tableResult.getSourceTableName())
				.targetSchemaName(tableResult.getTargetSchemaName()).targetTableName(tableResult.getTargetTableName())
				.ifSuccess(tableResult.getIfSuccess().get() ? YesOrNo.YES.getValue() : YesOrNo.NO.getValue())
				.syncCount(tableResult.getSyncCount().get()).syncBytes(BytesUnitUtils.bytesSizeToHuman(tableResult.getSyncBytes().get()))
				.createTime(new Date()).errorMsg(tableResult.getErrorMsg()).successMsg(tableResult.getSuccessMsg()).build());
	}
}
