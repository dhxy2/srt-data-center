package net.srt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 数据集成微服务
 *
 * @author zrx 985134801@qq.com
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class DataIntegrateApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataIntegrateApplication.class, args);
	}

}
