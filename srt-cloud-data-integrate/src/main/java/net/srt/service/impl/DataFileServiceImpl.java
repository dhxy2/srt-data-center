package net.srt.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import net.srt.convert.DataFileConvert;
import net.srt.entity.DataFileEntity;
import net.srt.framework.common.page.PageResult;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;
import net.srt.query.DataFileQuery;
import net.srt.vo.DataFileVO;
import net.srt.dao.DataFileDao;
import net.srt.service.DataFileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import srt.cloud.framework.dbswitch.common.util.StringUtil;

import java.util.List;

/**
 * 文件表
 *
 * @author zrx 985134801@qq.com
 * @since 1.0.0 2022-11-16
 */
@Service
@AllArgsConstructor
public class DataFileServiceImpl extends BaseServiceImpl<DataFileDao, DataFileEntity> implements DataFileService {

	@Override
	public PageResult<DataFileVO> page(DataFileQuery query) {
		IPage<DataFileEntity> page = baseMapper.selectPage(getPage(query), getWrapper(query));

		return new PageResult<>(DataFileConvert.INSTANCE.convertList(page.getRecords()), page.getTotal());
	}

	private LambdaQueryWrapper<DataFileEntity> getWrapper(DataFileQuery query) {
		LambdaQueryWrapper<DataFileEntity> wrapper = Wrappers.lambdaQuery();
		wrapper.like(StringUtil.isNotBlank(query.getName()), DataFileEntity::getName, query.getName());
		wrapper.like(StringUtil.isNotBlank(query.getType()), DataFileEntity::getType, query.getType());
		wrapper.eq(query.getFileCategoryId() != null, DataFileEntity::getFileCategoryId, query.getFileCategoryId());
		return wrapper;
	}

	@Override
	public void save(DataFileVO vo) {
		DataFileEntity entity = DataFileConvert.INSTANCE.convert(vo);
		entity.setProjectId(getProjectId());
		baseMapper.insert(entity);
	}

	@Override
	public void update(DataFileVO vo) {
		DataFileEntity entity = DataFileConvert.INSTANCE.convert(vo);
		entity.setProjectId(getProjectId());
		updateById(entity);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(List<Long> idList) {
		removeByIds(idList);
	}

}
