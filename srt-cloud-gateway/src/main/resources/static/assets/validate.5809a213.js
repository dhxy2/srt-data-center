import{a4 as s}from"./index.14bc3c0f.js";const a=(r,o,e)=>{o.length<4?e(new Error(s.global.t("error.password",{len:4}))):e()},l=r=>/^1[3456789]\d{9}$/.test(r);export{a,l as v};
